<?php
/**
 * Created by PhpStorm.
 * User: sthashankar
 * Date: 4/2/18
 * Time: 6:11 PM
 */
namespace BranchingAssessment;

interface BranchingAssessmentAlgorithm
{

    public function __construct($assessmentDefinition);

    public function setQuestionResponse($questionId,$isCorrect);

    public function getNextQuestionID();
}