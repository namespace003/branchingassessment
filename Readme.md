#Branching Assessment
The assessment is a part of hiring process in learnosity and submitted by Love Shankar Shrestha as a part of the program.

##Assumption
1. Each question is linked to another one
2. Result is one of the deciding factor for the next choice of question
3. Choice of every question lead to a specific path
4. Initial question is preconfigured 

##Approach
1. Every response is recorded 
2. Correct and incorrect response decide the type of path to be followed

##Enhancement
1. Step (Markes weight) is introduced to provide more control with in the decision making process of next question
2. A different starting point, may be versatile path approach would be more suitable for different mixture and starting the assignment

###Files</>
**BranchingAssignmentAlgorithm**
> It is a boilerplate for the assignment

**Assessment**
> Logics

**test**
> The process for the test is simulated with predefined condition