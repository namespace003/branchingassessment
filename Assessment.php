<?php
/**
 * Created by PhpStorm.
 * User: sthashankar
 * Date: 4/2/18
 * Time: 6:19 PM
 */
namespace BranchingAssessment;

include "BranchingAssessmentAlgorithm.php";

class Assessment implements BranchingAssessmentAlgorithm
{

    private $currentQuestion;
    private $assessmentDefinition;

    /**
     * Assessment constructor.
     * @param $assessmentDefinition
     *
     * key: for index purpose
     *
     *_id: defining the object
     *
     *step: understand the procedure and it might be helpful in connection logic
     *
     *result: true or false; store the current question output
     *
     *correct_next_id: value of next question id when the current question answer is correct
     *
     *incorrect_next_id: id of next question in case the correct question is incorrect
     */


    public function __construct($assessmentDefinition)
    {
//        parent::__construct($assessmentDefinition);
        $this->assessmentDefinition = json_decode($assessmentDefinition);

    }

    /*
     * task:: setting up the response
     *
     * result:: it is a deciding factor to  define the next question id. Based on the result next question id is decided
     *
     * */
    public function setQuestionResponse($questionId, $isCorrect)
    {
        $this->currentQuestion = $questionId;

        if ($isCorrect) {
            $this->assessmentDefinition->$questionId->result = true;
        } else {
            $this->assessmentDefinition->$questionId->result = false;
        }
    }

    public function getNextQuestionID()
    {
        // TODO: Implement getNextQuestionID() method.


        /*
         *task:: checking current question ID
         *
         * condition:: if current question is not set get the first Question
         *
         * todo :: further enhancement for random order generation
         *
        */
        $questionID = $this->currentQuestion;
        if ($questionID == null) {
            foreach ($this->assessmentDefinition as $first) {
                return $first->_id;
            }
        } else {

            /*
             * task:: deciding the next question id
             *
             * condition:: based on the result, define the next id or end the process
             *
             * todo:: multiple layer of question can be selected with more robust selection criteria
             *
             * */


            if ($this->assessmentDefinition->$questionID->result) {
                $next = $this->assessmentDefinition->$questionID->correct_next_id;
            } else {
                $next = $this->assessmentDefinition->$questionID->incorrect_next_id;
            }

            if ($next != null && $next != "" && isset($this->assessmentDefinition->$next)) {
                return $next;
            } else {
                return false;
            }
        }


    }


}