<?php
/**
 * Created by PhpStorm.
 * User: sthashankar
 * Date: 4/2/18
 * Time: 10:13 PM
 */
namespace BranchingAssessment;

include ("Assessment.php");

/*
 * check the response of the assessment
 *
 * looping the assessment using while loop with random response and accessing the path
 * */

class test
{

    function __construct()
    {

        $jsonData = '{
            "q-a": {
                "_id": "q-a",
                "step": "1",
                "result": "",
                "correct_next_id": "q-c",
                "incorrect_next_id": "q-b"
            },
            "q-b": {
                "_id": "q-b",
                "step": "1",
                "answer": "asdf",
                "result": "",
                "correct_next_id": "q-d",
                "incorrect_next_id": "q-d"
            },
             "q-c": {
                "_id": "q-c",
                "step": "1",
                "answer": "asdf",
                "result": "",
                "correct_next_id": "q-e",
                "incorrect_next_id": "q-f"
            },
            "q-d": {
                "_id": "q-d",
                "step": "1",
                "answer": "asdf",
                "result": "",
                "correct_next_id": "q-c",
                "incorrect_next_id": "q-c"
            },
            "q-e": {
                "_id": "q-e",
                "step": "1",
                "answer": "asdf",
                "result": "",
                "correct_next_id": "q-g",
                "incorrect_next_id": "q-f"
            },
            "q-f": {
                "_id": "q-f",
                "step": "1",
                "result": "",
                "correct_next_id": "q-h",
                "incorrect_next_id": "q-h"
            },
            "q-g": {
                "_id": "q-g",
                "step": "1",
                "result": "",
                "correct_next_id": "end",
                "incorrect_next_id": "end"
            },
            "q-h": {
                "_id": "q-h",
                "step": "1",
                "result": "",
                "correct_next_id": "q-g",
                "incorrect_next_id": "end"
            }
            
        }';


        $this->class = new Assessment($jsonData);

        $questionID = $this->class->getNextQuestionID();



        while ($questionID != false) {

            $response = rand(0, 1);

            printf("{$questionID} {$response} => ");

            $this->class->setQuestionResponse($questionID, $response);

            $questionID = $this->class->getNextQuestionID();


        }

        printf(" end of assessment \n");
    }
}

new test();


